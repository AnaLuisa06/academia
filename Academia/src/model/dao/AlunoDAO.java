/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;


import dao.conexao.Fabrica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.bean.Aluno;
import model.bean.Mensalidades;


/**
 *
 * @author Luh
 */
public class AlunoDAO {
    public int p;
     public void create(Aluno a){
         Connection con = Fabrica.getConexao();
         PreparedStatement stmt = null;
         
         
         try {
             stmt= con.prepareStatement("INSERT INTO sc_academia.aluno(nome, cpf, rg, dataNasc,sexo,telefone,email,rua,numero,bairro,cidade) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
             stmt.setString(1, a.getNome());
             stmt.setString(2, a.getCpf());
             stmt.setString(3, a.getRg());
             stmt.setDate(4, a.getDataNasc());
             stmt.setString(5, a.getSexo());
             stmt.setString(6, a.getTelefone());
             stmt.setString(7, a.getEmail());
             stmt.setString(8, a.getRua());
             stmt.setString(9, a.getNumero());
             stmt.setString(10, a.getBairro());
             stmt.setString(11, a.getCidade());
             
         
             stmt.executeUpdate();
             JOptionPane.showMessageDialog(null,"Salvo com sucesso");
         } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Erro ao salvar"+ex);
         }
}
     
     public int pesquisar(Aluno a){
         Connection con = Fabrica.getConexao();
         PreparedStatement stmt = null;
         
         
         try {
            stmt= con.prepareStatement("SELECT count(cpf) FROM sc_academia.aluno WHERE cpf=?");
             stmt.setString(1, a.getCpf());
             
           ResultSet rs = stmt.executeQuery();
            //a.setText(Integer.parseInt(rs.toString()));
             //a = Integer.parseInt(rs.getObject(1));
            for(;rs.next();){
                 p=Integer.parseInt(rs.getString(1));
             }
            
            
            
            //stmt.executeQuery();
            
         } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Erro na pesquisa"+ex);
         }
         return (p);
}
     
     public void update(Aluno a){
         Connection con = Fabrica.getConexao();
         PreparedStatement stmt = null;
         
         
         try {
             stmt= con.prepareStatement("UPDATE sc_academia.alno SET nome=?,cpf=?,rg=?,dataNasc=?,sexo=?,telefone=?,email=?,rua=?,numero=?,bairro=?,cidade=? WHERE cpf=?");
             stmt.setString(1, a.getNome());
             stmt.setString(2, a.getCpf());
             stmt.setString(3, a.getRg());
             stmt.setDate(4, a.getDataNasc());
             stmt.setString(5, a.getSexo());
             stmt.setString(6, a.getTelefone());
             stmt.setString(7, a.getEmail());
             stmt.setString(8, a.getRua());
             stmt.setString(9, a.getNumero());
             stmt.setString(10, a.getBairro());
             stmt.setString(11, a.getCidade());
             
         
             stmt.executeUpdate();
             JOptionPane.showMessageDialog(null,"Atualizado com sucesso");
         } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Erro ao atuaizar"+ex);
         }
}
     
     public List<Aluno> read(String mes) {

        Connection con = Fabrica.getConexao();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Aluno> alunos = new ArrayList<>();
        
        try {
            stmt = con.prepareStatement("SELECT  sc_academia.aluno.nome, sc_academia.aluno.telefone FROM sc_academia.aluno, sc_academia.mensalidades WHERE "
                    + "sc_academia.mensalidades.cpf == sc_academia.aluno.cpf && sc_academia.mensalidades.mes<?");
            stmt.setString(1, mes);
            rs = stmt.executeQuery();

            while (rs.next()) {

                Mensalidades mensalidade = new Mensalidades();
                Aluno aluno = new Aluno();
                aluno.setNome(rs.getString("Nome"));
                aluno.setTelefone(rs.getString("Telefone"));
                alunos.add (aluno);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MensalidadesDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 

        return alunos;

    }
     
}
