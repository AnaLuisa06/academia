/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;


import dao.conexao.Fabrica;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.bean.Aluno;
import model.bean.Mensalidades;

/**
 *
 * @author Luh
 */
public class MensalidadesDAO {
    public void create(Mensalidades a){
         Connection con = Fabrica.getConexao();
         PreparedStatement stmt = null;
         
         
         try {
             stmt= con.prepareStatement("INSERT INTO sc_academia.mensalidades(cpf,mes,data) VALUES(?,?,?)");
             stmt.setString(1, a.getCpf());
             stmt.setString(2, a.getMes());
             stmt.setDate(3, a.getData());
             
             
         
             stmt.executeUpdate();
             JOptionPane.showMessageDialog(null,"Salvo com sucesso");
         } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Erro ao salvar"+ex);
         }
    
    }   
    
    
    public List<Aluno> readForDesc(String mes) {

        Connection con = Fabrica.getConexao();
        
        PreparedStatement stmt = null;
        ResultSet rs = null;

        java.util.List<Mensalidades> me = new ArrayList<>();
java.util.List<Aluno> al = new ArrayList<>();
        try {
            stmt = con.prepareStatement("SELECT sc_academia.aluno.nome, sc_academia.telefoe FROM sc_academia.aluno, sc_academia.mensalidades WHERE sc_academia.mensalidades.cpf==sc_academia.aluno.cpf AND sc_academia.mensalidade.mes<?");
            stmt.setString(1, mes);
            
            rs = stmt.executeQuery();

            while (rs.next()) {

                Aluno a = new Aluno();

                a.setNome(rs.getString("Nome"));
                a.setTelefone(rs.getString("Telefone"));
                al.add(a);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MensalidadesDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 
    
        return al;
    
    }
}
