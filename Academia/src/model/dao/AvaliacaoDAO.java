/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;


import dao.conexao.Fabrica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.bean.Avaliacao;

/**
 *
 * @author Luh
 */
public class AvaliacaoDAO {
   public int p;
     public void create(Avaliacao av){
         Connection con = Fabrica.getConexao();
         PreparedStatement stmt = null;
         
         
         try {
             stmt= con.prepareStatement("INSERT INTO sc_academia.avaliacao(matricula, cpf, nome, dataAvaliacao,sexo,estatra,idade, massa,imc,pescoço,ombro,torax,abdomen,cintura,quadril,bracoE,antebracoE,coxaE,pernaE,bracoD,antebracoD,coxaD,pernaD,massaIdeal) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
             stmt.setInt(1, av.getMatricula());
             stmt.setString(2, av.getCpf());
             stmt.setString(3, av.getNome());
             stmt.setDate(4, av.getDataAvaliacao());
             stmt.setString(5, av.getSexo());
             stmt.setFloat(6, av.getEstatra());
             stmt.setInt(7, av.getIdade());
             stmt.setFloat(8, av.getMassa());
             stmt.setFloat(9, av.getImc());
             stmt.setFloat(10, av.getPescoco());
             stmt.setFloat(11, av.getOmbro());
             stmt.setFloat(12, av.getTorax());
             stmt.setFloat(13, av.getAbdomen());
             stmt.setFloat(14, av.getCintura());
             stmt.setFloat(15, av.getQuadril());
             stmt.setFloat(16, av.getBracoE());
             stmt.setFloat(17, av.getAntebracoE());
             stmt.setFloat(18, av.getCoxaE());
             stmt.setFloat(19, av.getPernaE());
             stmt.setFloat(20, av.getBracoD());
             stmt.setFloat(21, av.getAntebracoD());
             stmt.setFloat(22, av.getCoxaD());
             stmt.setFloat(23, av.getPernaD());
             stmt.setFloat(24, av.getMassaIdeal());
             
         
             stmt.executeUpdate();
             JOptionPane.showMessageDialog(null,"Salvo com sucesso");
         } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Erro ao salvar"+ex);
         }
}
     
     public int pesquisar(Avaliacao a){
         Connection con = Fabrica.getConexao();
         PreparedStatement stmt = null;
         
         
         try {
            stmt= con.prepareStatement("SELECT count(cpf) FROM sc_academia.aluno WHERE cpf=?");
             stmt.setString(1, a.getCpf());
             
           ResultSet rs = stmt.executeQuery();
            //a.setText(Integer.parseInt(rs.toString()));
             //a = Integer.parseInt(rs.getObject(1));
            for(;rs.next();){
                 p=Integer.parseInt(rs.getString(1));
             }
            
            
            
            //stmt.executeQuery();
            
         } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Erro na pesquisa"+ex);
         }
         return (p);
} 
}
