/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.sql.Date;

/**
 *
 * @author Luh
 */
public class Avaliacao {
   private int matricula;
   private String cpf;
   private String nome;
   private Date dataAvaliacao;
   private String sexo;
   private Float estatra;
   private int idade;
   private float massa;
   private float imc;
   private float pescoco;
   private float ombro;
   private float torax;
   private float abdomen;
   private float cintura;
   private float quadril;
   private float bracoE;
   private float antebracoE;
   private float coxaE;
   private float pernaE;
   private float bracoD;
   private float antebracoD;
   private float coxaD;
   private float pernaD;
   private float massaIdeal;

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataAvaliacao() {
        return dataAvaliacao;
    }

    public void setDataAvaliacao(Date datAvaliacao) {
        this.dataAvaliacao = datAvaliacao;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Float getEstatra() {
        return estatra;
    }

    public void setEstatra(Float estatara) {
        this.estatra = estatara;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public float getMassa() {
        return massa;
    }

    public void setMassa(float massa) {
        this.massa = massa;
    }

    public float getImc() {
        return imc;
    }

    public void setImc(float imc) {
        this.imc = imc;
    }

    public float getPescoco() {
        return pescoco;
    }

    public void setPescoco(float pescoco) {
        this.pescoco = pescoco;
    }

    public float getOmbro() {
        return ombro;
    }

    public void setOmbro(float ombro) {
        this.ombro = ombro;
    }

    public float getTorax() {
        return torax;
    }

    public void setTorax(float torax) {
        this.torax = torax;
    }

    public float getAbdomen() {
        return abdomen;
    }

    public void setAbdomen(float abdomen) {
        this.abdomen = abdomen;
    }

    public float getCintura() {
        return cintura;
    }

    public void setCintura(float cintura) {
        this.cintura = cintura;
    }

    public float getQuadril() {
        return quadril;
    }

    public void setQuadril(float quadril) {
        this.quadril = quadril;
    }

    public float getBracoE() {
        return bracoE;
    }

    public void setBracoE(float bracoE) {
        this.bracoE = bracoE;
    }

    public float getAntebracoE() {
        return antebracoE;
    }

    public void setAntebracoE(float antebracoE) {
        this.antebracoE = antebracoE;
    }

    public float getCoxaE() {
        return coxaE;
    }

    public void setCoxaE(float coxaE) {
        this.coxaE = coxaE;
    }

    public float getPernaE() {
        return pernaE;
    }

    public void setPernaE(float pernaE) {
        this.pernaE = pernaE;
    }

    public float getBracoD() {
        return bracoD;
    }

    public void setBracoD(float bracoD) {
        this.bracoD = bracoD;
    }

    public float getAntebracoD() {
        return antebracoD;
    }

    public void setAntebracoD(float antebracoD) {
        this.antebracoD = antebracoD;
    }

    public float getCoxaD() {
        return coxaD;
    }

    public void setCoxaD(float coxaD) {
        this.coxaD = coxaD;
    }

    public float getPernaD() {
        return pernaD;
    }

    public void setPernaD(float pernaD) {
        this.pernaD = pernaD;
    }

    public float getMassaIdeal() {
        return massaIdeal;
    }

    public void setMassaIdeal(float massaIdeal) {
        this.massaIdeal = massaIdeal;
    }
   
   
}
