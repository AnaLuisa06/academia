/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import dao.conexao.Fabrica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.bean.aluno;

/**
 *
 * @author Luh
 */
public class AlunoDAO {
    public void create(aluno a){
         Connection con = Fabrica.getConexao();
         PreparedStatement stmt = null;
         
         
         try {
             stmt= con.prepareStatement("INSERT INTO sc_bd.aluno(matricula,cpf,nome,email,telefone) VALUES(?,?,?,?,?)");
             stmt.setInt(1, a.getMatricula());
             stmt.setString(2, a.getCpf());
             stmt.setString(3, a.getNome());
             stmt.setString(4, a.getEmail());
             stmt.setString(5, a.getTelefone());
                   
             stmt.executeUpdate();
             JOptionPane.showMessageDialog(null,"Salvo com sucesso");
         } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null,"Erro ao salvar"+ex);
         }
}
}
